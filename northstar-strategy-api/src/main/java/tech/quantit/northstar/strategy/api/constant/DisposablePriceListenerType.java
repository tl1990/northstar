package tech.quantit.northstar.strategy.api.constant;

public enum DisposablePriceListenerType {
	/**
	 * 止盈
	 */
	TAKE_PROFIT,
	/**
	 * 止损
	 */
	STOP_LOSS;
}
